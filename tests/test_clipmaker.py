from datetime import datetime, timedelta
from clipmaker.clipmaker import generate_clip_groups


def test_generate_clip_groups():
    base_time = datetime(
        year=2020, month=1, day=1, hour=18, minute=0, second=0
    )

    clips: Dict[str, datetime] = {
        'clip2': base_time + timedelta(seconds=20),
        'clip1': base_time,
        'clip5': base_time + timedelta(minutes=40),
        'clip3': base_time + timedelta(seconds=40),
        'clip6': base_time + timedelta(minutes=40, seconds=20),
        'clip4': base_time + timedelta(minutes=20),
    }

    expected_output = [
        ['clip1', 'clip2', 'clip3'],
        ['clip4'],
        ['clip5', 'clip6']
    ]

    assert generate_clip_groups(clips) == expected_output
