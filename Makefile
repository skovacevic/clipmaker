test:
	python -m pytest

setup:
	pip install -r requirements.txt
	pip install -r dev-requirements.txt

mypy:
	mypy --ignore-missing-imports clipmaker/
