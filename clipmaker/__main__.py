import os
import argparse
from .clipmaker import make_montages_from_directory


def console_entry() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--input',
        '-i',
        dest='input_dir',
        default=os.getcwd(),
        type=str,
        help=(
            'Path to the directory where input clips are stored. '
            'Defaults to current working directory.'
        )
    )
    parser.add_argument(
        '--output',
        '-o',
        dest='output_dir',
        default=os.getcwd(),
        type=str,
        help=(
            'Path to the directory where montages will be stored. '
            'Defaults to current working directory.'
        )
    )
    args = parser.parse_args()
    make_montages_from_directory(
        input_dir=args.input_dir,
        output_dir=args.output_dir
    )


if __name__ == '__main__':
    console_entry()
