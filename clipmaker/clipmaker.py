from typing import List, Dict, Optional

import os
from uuid import uuid4
from collections import defaultdict
from datetime import datetime, timedelta
from moviepy.editor import VideoFileClip, concatenate_videoclips


def get_clips(path: str) -> Dict[str, datetime]:
    clips: Dict[str, datetime] = {}

    for item in os.listdir(path):
        full_path = os.path.join(path, item)

        if os.path.isfile(full_path) and full_path.endswith('.mp4'):
            clips[full_path] = get_file_create_time(full_path)
    return clips


def generate_clip_groups(
    clips: Dict[str, datetime],
    group_time_range: timedelta = timedelta(minutes=3)
) -> List[List[str]]:
    """Group clips by time of the creation.
    Every clip that is created within
    3 minute period (configurable via group_time_range) will be grouped
    together.

    Arguments:
        clips {Dict[str, datetime]} -- Clip path to clip create time mapping

    Keyword Arguments:
        group_time_range {timedelta} -- Time range of each group of clips

    Returns:
        List[List[str]] -- List of groups, each group is a list of clip paths
    """
    sorted_clips = sorted(clips.items(), key=lambda x: x[1])

    current_time_anchor: Optional[datetime] = None
    groups: Dict[datetime, List[str]] = defaultdict(list)
    for clip_path, time in sorted_clips:
        if (
            current_time_anchor is None or
            current_time_anchor + group_time_range < time
        ):
            groups[time].append(clip_path)
            current_time_anchor = time
            continue
        groups[current_time_anchor].append(clip_path)

    return list(groups.values())


def make_montage(clip_paths: List[str], output_dir: str) -> None:
    clips = list(map(lambda path: VideoFileClip(path), clip_paths))
    final_clip = concatenate_videoclips(clips)

    current_time = datetime.utcnow().replace(microsecond=0).isoformat()
    current_time = current_time.replace(':', '_')  # ffmpeg doesn't like `:`
    clip_name = f'{current_time}-montage.mp4'

    print(f'Making clip: `{clip_name}`...')
    full_montage_path = os.path.join(output_dir, clip_name)
    final_clip.write_videofile(full_montage_path)


def get_file_create_time(path: str) -> datetime:
    timestamp: float = os.path.getctime(path)
    return datetime.utcfromtimestamp(timestamp)


def make_montages_from_directory(input_dir: str, output_dir: str) -> None:
    print(f'Getting clips from: `{input_dir}`...')
    clips = get_clips(path=input_dir)

    if not clips:
        print(f'No clips found in: {input_dir}')
        return

    print('Generating montage groups...')
    groups = generate_clip_groups(clips=clips)

    for group in groups:
        print(f'Making montage from a group: {group}')
        if len(group) < 2:
            print('One clip? That one will be short... Git gud.')

        make_montage(clip_paths=group, output_dir=output_dir)
    print('Montages created...')
