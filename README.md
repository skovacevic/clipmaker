# Clipmaker

Combines multiple videos based on the time of creation.

## Requirements
1. python 3.5 and above

## Installation
1. Clone the repo
2. `python3 setup.py install`

## Usage
### As a command line tool

```
clipmaker --input samples/ --output result/

clipmaker -o ../result/
```

Run `clipmaker --help` for more info on arguments


### As a library
Example
```python
import os
import clipmaker

clipmaker.make_montages_from_directory(
    input_dir=os.getcwd(), output_dir="../output"
)
```
