import setuptools


with open('README.md', 'r') as f:
    long_description = f.read()


with open('requirements.txt', 'r') as f:
    requrements = f.read().splitlines()


setuptools.setup(
    name='clipmaker',
    version='0.1.0',
    author='Savo Kovacevic',
    author_email='savo.s.kovacevic@gmail.com',
    description='Combines clips into montage',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/skovacevic/clipmaker',
    packages=['clipmaker'],
    classifiers=[
        'Programming Language :: Python :: 3'
    ],
    entry_points={
        'console_scripts': ['clipmaker=clipmaker.__main__:console_entry']
    },
    install_requires=requrements,
    python_requires='>=3.6',
)
